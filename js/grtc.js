/**
 * G:RTC Javascript
 * Author: Marc Steele
 */

$(document).ready(function() {

	// State

	var currentState = 'unauthenticated';
	var username;
	var localStream;
	var otherUser;
	var peerConnection;

	// STATICs

	var STUN_SERVER = 'stun:stun.l.google.com:19302';

	// Audio setup handling

	function gotAudioStream(stream) {
		localStream = stream;
		currentState = 'idle';
		console.log('Audio stream configured.');
		setupVUMeter(stream);
        }

        function failedAudioStream(error) {
                alert('We ran into the following error trying to make this work: ' + error);
		conole.log('Failed to get audio stream.');
        }

	// PPM (by Alan Fleming)

	function setupVUMeter(stream) {

		// Meter falloff - it's dependant on the buffersize in the ScriptProcessor. Lower numbers = slower falloff

		var maxDecay = 0.15;

		// Last values. Used for ballistics

		var lastdbL;
		var lastdbR;
		var meterRange = -34;

		// Make sure we can do access the audio data

		var audioContext = null;
		var javascriptNode = null;

		if (typeof AudioContext !== 'undefined') {
			audioContext = new AudioContext();
			javascriptNode = audioContext.createScriptProcessor(1024, 2, 2);
		} else {
			audioContext = new webkitAudioContext();
			javascriptNode = audioContext.createJavaScriptNode(1024, 2, 2);
		}


		analyser = audioContext.createAnalyser();
		microphone = audioContext.createMediaStreamSource(stream);

		// Get a hold of the canvases

		var canvasL = $('#Lmeter')[0];
		var ctxL = canvasL.getContext('2d');
		var wL = canvasL.width;
		var hL = canvasL.height;

		var canvasR = $('#Rmeter')[0];
		var ctxR = canvasR.getContext('2d');
		var wR = canvasR.width;
		var hR = canvasR.height;

		var canvasMeterLegend = $('#meterLegend')[0];
		var ctxMeterLegend = canvasMeterLegend.getContext('2d');
		var wMeterLegend = canvasMeterLegend.width;
		var hMeterLegend = canvasMeterLegend.height;

		// Fill the left meter canvas first

		ctxL.fillStyle = '#555';
		ctxL.fillRect(0,0,wL,hL);

		// And now the right meter

		ctxR.fillStyle = '#555';
		ctxR.fillRect(0,0,wR,hR);

		// Let's draw the legend

		ctxMeterLegend.fillStyle = '#555';
		ctxMeterLegend.fillRect(0,0,wMeterLegend,hMeterLegend);
		ctxMeterLegend.fillStyle="white";
		ctxMeterLegend.font = "Bold ariel 22pt";
		ctxMeterLegend.textBaseline="middle"; 
		ctxMeterLegend.textAlign = "center";
	  
		ctxMeterLegend.fillText('1',wMeterLegend - ((wMeterLegend / meterRange * 30) * -1),5);
		ctxMeterLegend.fillText('2',wMeterLegend - ((wMeterLegend / meterRange * 26) * -1),5);
		ctxMeterLegend.fillText('3',wMeterLegend - ((wMeterLegend / meterRange * 22) * -1),5);
		ctxMeterLegend.fillText('4',wMeterLegend - ((wMeterLegend / meterRange * 18) * -1),5);
		ctxMeterLegend.fillText('5',wMeterLegend - ((wMeterLegend / meterRange * 14) * -1),5);
		ctxMeterLegend.fillText('6',wMeterLegend - ((wMeterLegend / meterRange * 10) * -1),5);
		ctxMeterLegend.fillStyle="red";
		ctxMeterLegend.fillText('7',wMeterLegend - ((wMeterLegend / meterRange * 6) * -1),5);

		// Wire in the audio

		microphone.connect(analyser);
		analyser.connect(javascriptNode);
		javascriptNode.connect(audioContext.destination);

		// Tie in the events

		document.addEventListener('dragover', function(evt) {
			evt.preventDefault();
		});

		javascriptNode.onaudioprocess = function (evt) {

			var outL = evt.outputBuffer.getChannelData(0);
			var intL = evt.inputBuffer.getChannelData(0);
			var maxL = 0;

			var outR = evt.outputBuffer.getChannelData(1);
			var intR = evt.inputBuffer.getChannelData(1);
			var maxR = 0;

			// Prevent feedback!

			for (var i = 0; i < intL.length; i++) {
				outL[i] = 0;
				maxL = intL[i] > maxL ? intL[i] : maxL;
			}

			for (var j = 0; j < intR.length; j++) {
				outR[j] = 0;
				maxR = intR[j] > maxR ? intR[j] : maxR;
			}

			// Convert from mangnitude to dB

			var dbL = (20*Math.log(Math.max(maxL,Math.pow(10,meterRange/20)))/Math.LN10);
			var dbR = (20*Math.log(Math.max(maxR,Math.pow(10,meterRange/20)))/Math.LN10);

			// Draw it on the canvas

			var gradL = ctxL.createLinearGradient(wL,0,0,0);
			gradL.addColorStop(0,'red');
			gradL.addColorStop(-10/meterRange,'red');
			gradL.addColorStop(-11/meterRange,'yellow');
			gradL.addColorStop(-16/meterRange,'yellow');
			gradL.addColorStop(-20/meterRange,'limegreen');
			gradL.addColorStop(-28/meterRange,'green');
			gradL.addColorStop(1,'darkgreen');
    
			var gradR = ctxR.createLinearGradient(wR,0,0,0);
			gradR.addColorStop(0,'red');
			gradR.addColorStop(-10/meterRange,'red');
			gradR.addColorStop(-11/meterRange,'yellow');
			gradR.addColorStop(-16/meterRange,'yellow');
			gradR.addColorStop(-20/meterRange,'limegreen');
			gradR.addColorStop(-28/meterRange,'green');
			gradR.addColorStop(1,'darkgreen');

			// Fill the background

			ctxL.fillStyle = '#555';
			ctxL.fillRect(0,0,wL,hL);
			ctxL.fillStyle = gradL;

			ctxR.fillStyle = '#555';
			ctxR.fillRect(0,0,wR,hR);
			ctxR.fillStyle = gradR;

			// Draw the rectangle

			if(((0-dbL) - (0-lastdbL)) > maxDecay) {
				ctxL.fillRect(0,0,wL-wL*((lastdbL-maxDecay)/meterRange),hL);
				lastdbL = lastdbL - maxDecay;
			} else {
				ctxL.fillRect(0,0,wL-wL*(dbL/meterRange),hL);
				lastdbL = dbL;
			}

			if(((0-dbR) - (0-lastdbR)) > maxDecay) {
				ctxR.fillRect(0,0,wR-wR*((lastdbR-maxDecay)/meterRange),hR);
				lastdbR = lastdbR - maxDecay;
			} else {
				ctxR.fillRect(0,0,wR-wR*(dbR/meterRange),hR);
				lastdbR = dbR;
			}


			// Create meter markings

			for(var k = 2; k < wL; k+=3){
				ctxL.clearRect( k, 0, 1, hL);
			};

			for(var k = 2; k < wL; k+=3){
				ctxR.clearRect( k, 0, 1, hR);
			};

			// Are we clipping?

			if(lastdbL > -10) {

				ctxL.fillStyle="red";
				ctxL.font = "Bold Arial 22pt";
				ctxL.textAlign = "right";
				ctxL.fillText('PEAK',wL-5,10);
			}

			if(lastdbR > -10) {

				ctxR.fillStyle="red";
				ctxR.font = "Bold Arial 22pt";
				ctxR.textAlign = "right";
				ctxR.fillText('PEAK',wR-5,10);
			}

		};


	}

	// Set up the web socket

	var webSocket = new WebSocket("ws://grtcdev.thisisglobal.com:1234/");

	// Messages from the web socket

	webSocket.onmessage = function(event) {

		// Log the inbound message

		console.log('From sever: ' + event.data);

		// Tackle the messages depending on our state

		if (currentState == 'authenticating') {

			// Authentication attempt

			if (event.data.indexOf('AUTH_OK') == 0) {

				// Close and clear the dialog

				$('#loginForm').dialog("close");
				$('#loginUsername').val('');
				$('#loginPassword').val('');

				// Set up the audio side of things

				navigator.getUserMedia = ( navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia );

				if (!navigator.getUserMedia) {
					alert('Sorry but your browser is not supported by G:RTC. Please upgrade to a newer version of Firefox or Chrome.');
					return;
				} else {
					navigator.getUserMedia({audio:true}, gotAudioStream, failedAudioStream);
				}

				// Make us idle

				currentState = 'idle';

			} else if (event.data.indexOf('AUTH_FAIL')) {
				alert('Failed to log in. Check your username and password, then try again.');
			}

		} else if (event.data.indexOf('USERS') == 0) {

			// User list update
			// Strip out the header

			var raw_user_list = event.data.substring(6);
			var user_list = JSON.parse(raw_user_list);

			// Remove the old entries

			$('#online_users').empty();

			// Put the new entries in

			for (var i=0; i < user_list.length; i++) {
				if (user_list[i] !== username) {
					$('#online_users').append('<li>' + user_list[i] + '</li>');
				}
			}

			// Perpare for the next update
			// Not needed. Should be sent by the server on any change.
			//setTimeout(triggerUserListRefresh, 60000);

		} else if (event.data.indexOf('RINGING') == 0) {
		
			// Blankety blank!

		} else if (event.data.indexOf('RING') == 0) {

			// Inbound ringing
			// Find out who the other user is

			var caller = event.data.substring(5);

			// Check we can accept it

			if (currentState !== 'idle') {
				webSocket.send('FAIL Engaged');
			}

			// Change our state

			currentState = 'ringing';
			otherUser = caller;
			$('#callStatus').text('RING! RING!');
			$('#dial').attr('disabled', 'disabled');
			$('#hangup').removeAttr('disabled');
			$('#answer').removeAttr('disabled');

		} else if (event.data.indexOf('ACCEPT') == 0) {

			// Inbound accept message
			// Setup the peer connection if we need to

			if (peerConnection == undefined) {
				setupPeerConnection();
			}

			peerConnection.createAnswer(function(description) {
				description.sdp = insertBandwidthSetting(description.sdp);
				console.log(description);
				peerConnection.setLocalDescription(description);
				webSocket.send('ACCEPT ' + otherUser + ' ' + JSON.stringify({"sdp": description}));
			});

			// Handle any inbound messages


			var remoteMessage = event.data;
			remoteMessage = remoteMessage.substring(remoteMessage.indexOf(' ') + 1);
			remoteMessage = remoteMessage.substring(remoteMessage.indexOf(' ') + 1);			
			
			console.log('Parsing JSON: ' + remoteMessage);
			remoteJSONMessage = JSON.parse(remoteMessage);

			if (remoteJSONMessage.sdp) {
				var remoteSessionDescription = new RTCSessionDescription(remoteJSONMessage.sdp);
				console.log(remoteSessionDescription);
				peerConnection.setRemoteDescription(remoteSessionDescription);
			} else if (remoteJSONMessage.candidate) {
				peerConnection.addIceCandidate(new RTCIceCandidate(remoteJSONMessage.candidate));
			}

			// Change our state (just in case)

			currentState = 'incall';
			$('#callStatus').text('IN CALL');

		} else if (event.data.indexOf('HANGUP') == 0) {

			// Hang up any call (if it's live)

			if (currentState == 'incall') {

				if (peerConnection !== undefined) {
					peerConnection.close();
					peerConnection = undefined;
				}

			}

			// Change our state

			currentState = 'idle';
			$('#dial').removeAttr('disabled');
                        $('#hangup').attr('disabled', 'disabled');
                        $('#answer').attr('disabled', 'disabled');
			$('#callStatus').text('IDLE');

		}

	}

	// Setup the login form

	$('#loginForm').dialog({
		autoOpen: false,
		//height: 300,
		//width: 280,
		modal: true,
		closeOnEscape: false,
		buttons : {
			"Log In" : function() {

				// Check the credentials with the server

				username = $('#loginUsername').val();
				var password = $('#loginPassword').val();

				currentState = 'authenticating';
				webSocket.send('AUTHENTICATE ' + username + ' ' + password);
				console.log('Login attempt as ' + username + '.');

			}
		}
	});

	// Display the login form

	$('#loginForm').dialog("open");

	// Disable the call control buttons

	$('#answer').attr('disabled','disabled');
	$('#hangup').attr('disabled','disabled');

	// Make the user list selectable

	$('#online_users').selectable({
		selected: function() {

			// Check our state

			if (currentState !== 'idle') {
				return;
			}

			// Update the selected user

			otherUser = $(this).text();
			console.log('Changed other user to ' + otherUser);

		}
	});

	// User list refresh trigger

	function triggerUserListRefresh() {
		webSocket.send("USERS");
		console.log("Triggered user list refresh.");
	}

	// Outbound dial

	$('#dial').click(function() {

		// Make sure we've got a user

		if (otherUser == undefined) {
			alert('You need to select someone to dial!');
			return;
		}

		// Make the call

		webSocket.send('DIAL ' + otherUser);
		console.log('Dialing ' + otherUser);

		// Change our status

		$('#callStatus').text('DIALING');
		currentState = 'dialing';

		// Change the buttons

		$('#dial').attr('disabled', 'disabled');
		$('#hangup').removeAttr('disabled');

	});

	// Accept inbound

	function answerInboundCall() {

		// Setup the WebRTC session

                setupPeerConnection();
                peerConnection.createOffer(function(description) {
			description.sdp = insertBandwidthSetting(description.sdp);
                        peerConnection.setLocalDescription(description);
			console.log(description);
                        webSocket.send('ACCEPT ' + otherUser + ' ' + JSON.stringify({"sdp": description}));
                });

                // Change state

                currentState = 'incall';
                $('#callStatus').text('IN CALL');
                $('#answer').attr('disabled', 'disabled');

	}

	$('#answer').click(answerInboundCall);

	// Peer Connection Setup

	function setupPeerConnection() {

		if (typeof RTCPeerConnection !== 'undefined') {
			peerConnection = new RTCPeerConnection({'iceServers': [{'url': STUN_SERVER}]});
		} else if (typeof webkitRTCPeerConnection !== 'undefined') {
			peerConnection = new webkitRTCPeerConnection({'iceServers': [{'url': STUN_SERVER}]});
		} else if (typeof mozRTCPeerConnection !== 'undefined') {
			peerConnection = new mozRTCPeerConnection({'iceServers': [{'url': STUN_SERVER}]});
		}

                peerConnection.onicecandidate = function(event) {
                        webSocket.send('ACCEPT ' + otherUser + ' ' + JSON.stringify({"candidate": event.candidate}));
                        console.log('Told other user about ICE candidate ' + event.candidate);
                }

		peerConnection.onaddstream = function(event) {
			var remoteAudio = $('#remoteAudio')[0];
			remoteAudio.src = URL.createObjectURL(event.stream);
			remoteAudio.autoplay = true;
		}

                peerConnection.addStream(localStream);

	}

	// Bandwidth setting

	function insertBandwidthSetting(sdp) {

		// Figure out what bitrate and channel count we're using

		var selectedBitrate = $('#bitrate').val();
		var stereo = selectedBitrate > 64 ? 1 : 0;

		// Insert bandwidth setting

		var sdpLines = sdp.split('\r\n');
		var newSdp = '';

		for (var i=0; i < sdpLines.length; i++) {

			// Update the bandwidth line

			if (sdpLines[i].indexOf('a=fmtp') == 0) {

				sdpLines[i] = sdpLines[i].substring(0, sdpLines[i].indexOf(' '));
				sdpLines[i] = sdpLines[i] + ' cbr=1; stereo=' + stereo + '; maxaveragebitrate=' + (1000 * selectedBitrate);

			}


			// Decoder time

			if (sdpLines[i].indexOf('a=maxptime') == 0) {
				sdpLines[i] = 'a=minptime:60';
			}

			// Slap the next line in

			newSdp = newSdp + sdpLines[i] + '\r\n';

			// Allow up to 65535kbps. Basically a silly value.

			if (sdpLines[i].indexOf('a=mid') == 0) {
				newSdp = newSdp + 'b=65535\r\n';
			}
		}

		return newSdp.substring(0, newSdp.length-2);

	}

	// Hangup

	$('#hangup').click(function() {

		// Hang up any call (if it's live)

                if (peerConnection !== undefined) {
                        peerConnection.close();
                        peerConnection = undefined;
                }

                webSocket.send('HANGUP ' + otherUser);

                // Change our state

               	currentState = 'idle';
                $('#dial').removeAttr('disabled');
                $('#hangup').attr('disabled', 'disabled');
                $('#answer').attr('disabled', 'disabled');
		$('#callStatus').text('IDLE');

	});

});
